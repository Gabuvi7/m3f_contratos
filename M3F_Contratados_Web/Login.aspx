﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/M3F.Master" CodeBehind="Login.aspx.vb" Inherits="M3F_Contratados_Web.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlForm" runat="server">

        <table class="CSS_TBL_LOGIN" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td colspan="2" align="center" class="RRHH_VENTANA_CONTENIDO newColor">INICIO DE SESIÓN
                </td>
            </tr>
            <tr>
                <td align="right" style="height: 25px">Usuario: &nbsp;</td>
                <td>
                    <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="height: 25px">Contraseña &nbsp;</td>
                <td>
                    <asp:TextBox ID="txtPwd" TextMode="Password" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblError" ForeColor="red" runat="server"></asp:Label>
                    <br>
                    <asp:Button ID="cmdAceptar" class="Boton" runat="server" Text="Aceptar" />

                </td>
            </tr>
        </table>

    </asp:Panel>
    <asp:Panel ID="pnlOut" runat="server" Visible="false">

        <table class="CSS_TBL_LOGIN" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td colspan="2" align="center" class="RRHH_VENTANA_CONTENIDO">FUERA DE SERVICIO
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblOUT" ForeColor="red" runat="server" Text="ATENCION!!! EL sitio se encuentra momentáneamente fuera de servicio. Por favor intente entrar mas tarde."></asp:Label>
                    <br>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
