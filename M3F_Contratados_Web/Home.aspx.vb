﻿Imports M3F_Contratados_Web.FuncionesComunes
Imports System.Data.SqlClient

Public Class Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not ObtenerResultado("[SP_SYS_CAMBIAR_CLAVE] '" & Session("stUser") & "'") = "OK" Then
            Response.Redirect(fnURL(Request.ServerVariables("SERVER_NAME"), "CambiarClave.aspx"))
        End If
    End Sub

End Class