﻿Imports M3F_Contratados_Web.FuncionesComunes
Imports System.Data.SqlClient
Public Class RptPlanta
    Inherits System.Web.UI.Page

    Dim iDiasModificar As String
    Dim Separador As String
    Dim sortView As DataView
    Dim border As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim lit As New Literal
            lit.Text = "<script>fnIniciar(" & Session("USR_PERFIL") & ")</script>"
            pnlResultado.Controls.Add(lit)
        Else
            If hdEjecutar.Value = "1" Then Reporte()
        End If
    End Sub

    Protected Sub btnCargar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCargar.Click
        hdOrdenCol.Value = "1"
        hdOrdenTipo.Value = "asc"
    End Sub

    Function Reporte() As Boolean
        Dim dsResult As DataSet
        Dim stError As String
        Dim vec(100) As String
        If EjecutarRPT(dsResult, stError) Then
            pnlResultado.Controls.Add((CargarTabla(dsResult, 100000, vec, border)))
        Else
            lblError.Text = stError
        End If
    End Function

    Function EjecutarRPT(ByRef dsResult As DataSet, ByRef stError As String) As Boolean
        Dim sSql As String
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand()


        dsResult = New DataSet
        Try
            EjecutarRPT = True
            Using RRHHConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("M3FConnectionString").ConnectionString.ToString)
                RRHHConn.Open()
                sSql = "Exec SP_Reporte_PlantaContratada @User"

                cmd.CommandText = sSql
                cmd.CommandType = CommandType.Text
                cmd.Connection = RRHHConn
                cmd.Parameters.AddWithValue("@User", SqlDbType.VarChar)
                cmd.Parameters("@User").Value = Session("stUser")

                'cmd.Parameters.AddWithValue("@Desde", SqlDbType.Int)
                'cmd.Parameters("@Desde").Value = txtDesde.Text


                'cmd.Parameters.AddWithValue("@Hasta", SqlDbType.Int)
                'cmd.Parameters("@Hasta").Value = txtHasta.Text

                'cmd.Parameters.AddWithValue("@Oficina", SqlDbType.Int)
                'cmd.Parameters("@Oficina").Value = ddlOficinas.SelectedValue

                'If ddlNombreApellido.SelectedValue = -1 Then

                '    cmd.Parameters.AddWithValue("@Legajo", SqlDbType.VarChar)
                '    cmd.Parameters("@Legajo").Value = txtLegajo.Text

                '    cmd.Parameters.AddWithValue("@NombreApellido", SqlDbType.VarChar)
                '    cmd.Parameters("@NombreApellido").Value = ""

                'Else
                '    cmd.Parameters.AddWithValue("@Legajo", SqlDbType.VarChar)
                '    cmd.Parameters("@Legajo").Value = ddlNombreApellido.SelectedValue

                '    cmd.Parameters.AddWithValue("@NombreApellido", SqlDbType.VarChar)
                '    cmd.Parameters("@NombreApellido").Value = ""


                'End If


                'cmd.Parameters.AddWithValue("@Grupo", SqlDbType.Int)
                'cmd.Parameters("@Grupo").Value = ddlGrupo.SelectedValue




                da.SelectCommand = cmd

                da.Fill(dsResult)
                border = ConfigurationManager.AppSettings("RHM3F_RPT_ORDEN") = "Y"

                If border Then
                    sortView = dsResult.Tables(0).DefaultView
                    If Not hdOrdenCol.Value = "" Then
                        border = True '  ConfigurationManager.AppSettings("RHMVL_RPT_ORDEN") = "Y"
                        sortView.Sort = dsResult.Tables(0).Columns(CInt(hdOrdenCol.Value) - 1).ColumnName & " " & hdOrdenTipo.Value

                    End If

                End If

            End Using

        Catch ex As Exception
            stError = Err.Description
            EjecutarRPT = False
        End Try
    End Function

End Class