﻿Imports M3F_Contratados_Web.FuncionesComunes
Public Class M3F
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("stUser") = "" Then
            pnlMenu.Visible = False
            If InStr(LCase(Request.Url.ToString), "login.aspx") = 0 And InStr(LCase(Request.Url.ToString), "default.aspx") = 0 Then
                Response.Redirect(fnURL(Request.ServerVariables("SERVER_NAME"), "login.aspx"))
            End If
        End If
        If Not Request("Legajo") = "" Then pnlTop.Visible = False
    End Sub

    Protected Sub btnSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Session("stUser") = ""
        Session.Abandon()
        Response.Redirect(fnURL(Request.ServerVariables("SERVER_NAME"), "Login.aspx"))
    End Sub

    Function VErificarPErfil(ByVal stOpcion As String, ByVal stUSer As String) As Boolean
        Dim stStatus As String = ""
        Try
            Dim ds As New DataSet
            Dim stQuery As String
            stQuery = "exec SP_SYS_WEB_VERIFICAR '" & stOpcion & "','" & stUSer & "'"
            ds = GetSQLDataSet(stQuery, stStatus)
            If stStatus = "OK" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Response.Write("Error:" & Err.Description)
            Return False
        End Try
    End Function
End Class