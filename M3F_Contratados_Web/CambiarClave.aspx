﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/M3F.Master" CodeBehind="CambiarClave.aspx.vb" Inherits="M3F_Contratados_Web.CambiarClave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <center>


        <div class="Titulo_Pagina">CAMBIAR CONTRASEÑA</div>
        <br>
        <font color="red">ATENCION: ES IMPORTANTE QUE CAMBIES TU CONTRASEÑA POR UNA NUEVA, LA MISMA, DEBERA TENER AL MENOS 5 CARACTERES.</font>
        <asp:Panel ID="pnlDatos" runat="server">
            <table align="center" class="TBL_FORMA">
                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" CssClass="FORMA_CAMPO" runat="server" Text="Contraseña Anterior"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtClaveAnterior" TextMode="Password" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" CssClass="FORMA_CAMPO" runat="server" Text="Nueva Contraseña"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtClaveNueva" TextMode="Password" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label4" CssClass="FORMA_CAMPO" runat="server" Text="Repita Nueva Contraseña"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtClaveConfirmar" TextMode="Password" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnCargar" runat="server" CssClass="Boton" Text="Guardar" ValidationGroup="1" />
                        <input type="button" value="Cancelar" class="Boton" onclick="javascript: window.location = 'home.aspx'" />
                    </td>
                </tr>
            </table>
            <br>
            <asp:Label ID="lblError" CssClass="FORMA_CAMPO" ForeColor="Red" runat="server" Text=""></asp:Label>

        </asp:Panel>
        <asp:Panel ID="pnlMensaje" Visible="false" runat="server">
            <br>
            <br>
            <asp:Label ID="lblMensaje" CssClass="FORMA_CAMPO" ForeColor="Red" runat="server" Text="La contraseña se cambio con éxito"></asp:Label>
            <br>
            <br>
            <a href="Home.aspx">Inicio</a>
        </asp:Panel>

    </center>
</asp:Content>
