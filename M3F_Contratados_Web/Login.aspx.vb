﻿Imports M3F_Contratados_Web.FuncionesComunes
Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtUser.Focus()
    End Sub

    Protected Sub cmdAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdAceptar.Click
        If IsPostBack Then
            If fnLogin(txtUser.Text, txtPwd.Text) Then
                pnlForm.Visible = False
                Response.Redirect(fnURL(Request.ServerVariables("SERVER_NAME"), "Home.aspx"))
            Else
                lblError.Text = "Usuario o contraseña inválida"
            End If
        End If
    End Sub

    Function fnLogin(ByVal stUser As String, ByVal stPass As String) As Boolean
        Dim stStatus As String = ""
        lblError.Text = ""
        Try
            Dim ds As New DataSet
            Dim stQuery As String
            stQuery = "exec [SP_SYS_Login] '" & Left(stUser, 20) & "','" & Left(stPass, 20) & "'"
            ds = GetSQLDataSet(stQuery, stStatus)

            If stStatus = "OK" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Session("stUser") = ds.Tables(0).Rows(0).Item("Usuario")
                    Session("stUserName") = ds.Tables(0).Rows(0).Item("Nombre")
                    Session("USR_OFICINA") = ds.Tables(0).Rows(0).Item("OficinaDefecto")
                    Session("USR_PERFIL") = ds.Tables(0).Rows(0).Item("Perfil")
                    Return True
                Else
                    Return False
                End If
            Else
                Response.Write(stStatus)
            End If
        Catch ex As Exception
            Response.Write(Err.Description)
            Return False
        End Try

    End Function

End Class