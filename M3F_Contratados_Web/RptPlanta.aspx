﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/M3F.Master" CodeBehind="RptPlanta.aspx.vb" Inherits="M3F_Contratados_Web.RptPlanta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container pt-3">
        Reporte de planta
    </div>

    <div class="container-fluid">
        <input type="hidden" runat="server" id="hdOrdenCol" name="hdOrdenCol" value="1" />
        <input type="hidden" runat="server" id="hdOrdenTipo" name="hdOrdenTipo" value="asc" />
        <input type="hidden" runat="server" id="hdEjecutar" name="hdEjecutar" value="1" />
        <asp:Button ID="btnCargar" CssClass="Boton" runat="server" Text="Consultar" OnClientClick="ExecuteRPT('1')" />
    </div>

    <asp:Panel ID="pnlResultado" runat="server"></asp:Panel>

    <asp:Label ID="lblError" runat="server"></asp:Label>
</asp:Content>
