﻿Imports M3F_Contratados_Web.FuncionesComunes
Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Redirect(fnURL(Request.ServerVariables("SERVER_NAME"), "Login.aspx"))
    End Sub

End Class