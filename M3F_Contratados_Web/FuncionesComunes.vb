﻿Imports System.Data.SqlClient
Public Class FuncionesComunes
    Public Shared Function fnURL(ByVal stURLOrigen As String, ByVal stURLDetino As String) As String
        Dim stServerLocal As String
        Dim stDestino As String
        stDestino = ConfigurationManager.AppSettings("RHMVL_SITIO_WEB")
        stServerLocal = ConfigurationManager.AppSettings("RHMVL_SERVER_LOCAL")
        If stServerLocal = "" Then stServerLocal = "10.0.14"

        If InStr(stURLOrigen, stServerLocal) > 0 Or InStr(LCase(stURLOrigen), "localhost") > 0 Then
            fnURL = stURLDetino
        Else
            fnURL = stDestino & stURLDetino
        End If
    End Function

    Public Shared Function GetSQLDataSet(ByVal stQuery1 As String, ByRef stStatus As String) As Data.DataSet
        Dim ds As New Data.DataSet
        Try
            Using cnn As New SqlConnection(ConfigurationManager.ConnectionStrings("M3FConnectionString").ConnectionString.ToString)
                cnn.Open()
                Dim da As New SqlDataAdapter
                Dim cmd As New SqlCommand(stQuery1, cnn)
                da.SelectCommand = cmd
                da.Fill(ds)

                stStatus = "OK"
            End Using

            Return ds
        Catch
            stStatus = Err.Description
        End Try
    End Function

    Public Shared Function ObtenerResultado(ByVal sQuery As String) As String
        Dim stStatus As String = ""
        Dim i As Integer
        Try
            Dim ds As New DataSet
            Dim stResult As String = ""
            ds = GetSQLDataSet(sQuery, stStatus)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                stResult = stResult & ds.Tables(0).Rows(i).Item(0)
            Next
            ObtenerResultado = stResult
        Catch ex As Exception
            ObtenerResultado = ""
        End Try
    End Function



    Public Shared Function CargarTabla(ByRef ds As DataSet, ByVal Max As Integer, ByRef vec() As String, ByVal bOrden As Boolean, Optional ByVal stRef As String = "") As WebControls.Table

        Dim j As Integer
        Dim iFin As Integer
        Dim i As Integer
        Dim wcRow As New TableRow
        Dim tblShow As New WebControls.Table
        Dim bTOtales As Boolean = False
        'Dim vecTot(ds.Tables(0).Columns.Count) As Double
        Dim vecTot(ds.Tables(0).Columns.Count) As Double


        For i = 0 To ds.Tables(0).Columns.Count - 1
            vecTot(i) = 0
        Next
        tblShow.CssClass = "TablsResult table-responsive"

        For i = 0 To ds.Tables(0).Columns.Count - 1
            Dim wcCell As New TableCell
            wcCell.Text = Replace(ds.Tables(0).Columns(i).Caption, "_", " ")
            If bOrden Then wcCell.Text = wcCell.Text & "<br><a href='JAVASCRIPT:sortResult(" & i & ",0)'><img src='images/UPGRAY.GIF'></a><a href='JAVASCRIPT:sortResult(" & i & ",1)'><img src='images/DOWNGRAY.GIF'></a>"

            wcCell.CssClass = "TablaResultHeader"
            wcRow.Controls.Add(wcCell)
        Next i
        tblShow.Controls.Add(wcRow)
        iFin = ds.Tables(0).Rows.Count - 1

        For j = 0 To iFin

            Dim wcRow2 As New TableRow
            For i = 0 To ds.Tables(0).Columns.Count - 1
                Dim wcCell As New TableCell
                wcCell.CssClass = "TablaResultCell"
                If Not Microsoft.VisualBasic.IsDBNull(ds.Tables(0).Rows(j).Item(i)) Then

                    wcCell.Text = ds.Tables(0).Rows(j).Item(i)
                    If i = 0 Then
                        If Not stRef = "" Then
                            wcCell.Text = " <a href='" & stRef & "" & ds.Tables(0).Rows(j).Item(i) & "'>" & ds.Tables(0).Rows(j).Item(i) & " </a>"
                        End If
                    End If

                    Try
                        If i <= vec.Length - 1 Then
                            Select Case LCase(vec(i))
                                Case "suma"

                                    If IsNumeric(ds.Tables(0).Rows(j).Item(i)) Then
                                        wcCell.Text = FormatNumber(ds.Tables(0).Rows(j).Item(i))
                                        wcCell.Text = Left(wcCell.Text, Len(wcCell.Text) - 3)
                                        vecTot(i) = vecTot(i) + ds.Tables(0).Rows(j).Item(i)
                                        bTOtales = True
                                        wcCell.CssClass = "TablaResultCell_" & LCase(vec(i))
                                    End If
                                Case "suma$"
                                    If IsNumeric(ds.Tables(0).Rows(j).Item(i)) Then
                                        wcCell.Text = FormatNumber(ds.Tables(0).Rows(j).Item(i), 2)

                                        vecTot(i) = vecTot(i) + ds.Tables(0).Rows(j).Item(i)
                                        bTOtales = True
                                        wcCell.CssClass = "TablaResultCell_" & LCase(vec(i))
                                    End If

                            End Select

                        End If
                    Catch ex As Exception

                    End Try



                Else
                    wcCell.Text = ""
                End If




                wcRow2.Controls.Add(wcCell)
            Next
            wcRow2.CssClass = "TablaResultRow_" & (j Mod 2) + 1
            tblShow.Controls.Add(wcRow2)
        Next

        If bTOtales Then
            Dim wcRow2 As New TableRow

            For i = 0 To ds.Tables(0).Columns.Count - 1
                Dim wcCell As New TableCell

                If i <= vec.Length - 1 Then
                    Select Case LCase(vec(i))
                        Case "suma"

                            wcCell.Text = FormatNumber(CDbl(vecTot(i)))
                            wcCell.Text = Left(wcCell.Text, Len(wcCell.Text) - 3)
                            wcCell.Text = "Total " & Replace(ds.Tables(0).Columns(i).Caption, "_", " ") & "<br>" & wcCell.Text
                        Case "suma$"

                            wcCell.Text = FormatNumber(CDbl(vecTot(i)), 2)

                            wcCell.Text = "Total " & Replace(ds.Tables(0).Columns(i).Caption, "_", " ") & "<br>" & wcCell.Text

                    End Select

                End If
                wcCell.CssClass = "TablaResultTotalCell"
                wcRow2.Controls.Add(wcCell)

            Next
            tblShow.Controls.Add(wcRow2)
        End If


        Return tblShow


    End Function


    Public Shared Function CargarTabla(ByRef dv As DataView, ByRef ds As DataSet, ByVal Max As Integer, ByRef vec() As String, ByVal bOrden As Boolean, Optional ByVal stRef As String = "") As WebControls.Table

        Dim j As Integer
        Dim iFin As Integer
        Dim i As Integer
        Dim wcRow As New TableRow
        Dim tblShow As New WebControls.Table
        Dim bTOtales As Boolean = False
        'Dim vecTot(ds.Tables(0).Columns.Count) As Double
        Dim vecTot(ds.Tables(0).Columns.Count) As Double


        For i = 0 To ds.Tables(0).Columns.Count - 1
            vecTot(i) = 0
        Next
        tblShow.CssClass = "TablsResult table-responsive"

        For i = 0 To ds.Tables(0).Columns.Count - 1
            Dim wcCell As New TableCell
            wcCell.Text = Replace(ds.Tables(0).Columns(i).Caption, "_", " ")
            If bOrden Then wcCell.Text = wcCell.Text & "<br><a href='JAVASCRIPT:sortResult(" & i & ",0)'><img src='images/UPGRAY.GIF'></a><a href='JAVASCRIPT:sortResult(" & i & ",1)'><img src='images/DOWNGRAY.GIF'></a>"

            wcCell.CssClass = "TablaResultHeader"
            wcRow.Controls.Add(wcCell)
        Next i
        tblShow.Controls.Add(wcRow)
        iFin = ds.Tables(0).Rows.Count - 1


        j = 0
        For Each dr As DataRowView In dv



            Dim wcRow2 As New TableRow
            For i = 0 To ds.Tables(0).Columns.Count - 1
                Dim wcCell As New TableCell
                wcCell.CssClass = "TablaResultCell"
                If Not Microsoft.VisualBasic.IsDBNull(dr.Item(i)) Then

                    wcCell.Text = dr.Item(i)
                    If i = 0 Then
                        If Not stRef = "" Then
                            wcCell.Text = " <a href='" & stRef & "" & dr.Item(i) & "'>" & dr.Item(i) & " </a>"
                        End If
                    End If

                    Try
                        If i <= vec.Length - 1 Then
                            Select Case LCase(vec(i))
                                Case "suma"

                                    If IsNumeric(dr.Item(i)) Then
                                        wcCell.Text = FormatNumber(dr.Item(i))
                                        wcCell.Text = Left(wcCell.Text, Len(wcCell.Text) - 3)
                                        vecTot(i) = vecTot(i) + dr.Item(i)
                                        bTOtales = True
                                        wcCell.CssClass = "TablaResultCell_" & LCase(vec(i))
                                    End If
                                Case "suma$"
                                    If IsNumeric(ds.Tables(0).Rows(j).Item(i)) Then
                                        wcCell.Text = FormatNumber(dr.Item(i), 2)

                                        vecTot(i) = vecTot(i) + dr.Item(i)
                                        bTOtales = True
                                        wcCell.CssClass = "TablaResultCell_" & LCase(vec(i))
                                    End If

                            End Select

                        End If
                    Catch ex As Exception

                    End Try



                Else
                    wcCell.Text = ""
                End If




                wcRow2.Controls.Add(wcCell)
            Next
            wcRow2.CssClass = "TablaResultRow_" & (j Mod 2) + 1
            tblShow.Controls.Add(wcRow2)

            j = j + 1
        Next

        If bTOtales Then
            Dim wcRow2 As New TableRow

            For i = 0 To ds.Tables(0).Columns.Count - 1
                Dim wcCell As New TableCell

                If i <= vec.Length - 1 Then
                    Select Case LCase(vec(i))
                        Case "suma"

                            wcCell.Text = FormatNumber(CDbl(vecTot(i)))
                            wcCell.Text = Left(wcCell.Text, Len(wcCell.Text) - 3)
                            wcCell.Text = "Total " & Replace(ds.Tables(0).Columns(i).Caption, "_", " ") & "<br>" & wcCell.Text
                        Case "suma$"

                            wcCell.Text = FormatNumber(CDbl(vecTot(i)), 2)

                            wcCell.Text = "Total " & Replace(ds.Tables(0).Columns(i).Caption, "_", " ") & "<br>" & wcCell.Text

                    End Select

                End If
                wcCell.CssClass = "TablaResultTotalCell"
                wcRow2.Controls.Add(wcCell)

            Next
            tblShow.Controls.Add(wcRow2)
        End If


        Return tblShow


    End Function
End Class
