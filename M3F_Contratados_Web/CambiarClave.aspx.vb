﻿Imports System.Data.SqlClient
Imports M3F_Contratados_Web.FuncionesComunes
Public Class CambiarClave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub btnCargar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCargar.Click
        Dim stError As String = ""

        If ClambiarClave(stError) Then
            pnlDatos.Visible = False
            pnlMensaje.Visible = True
        Else
            lblError.Text = stError
        End If

    End Sub
    Function ClambiarClave(ByRef stError As String) As Boolean
        Dim sSql As String
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand()
        Dim dsResult As New DataSet
        Try


            ClambiarClave = True

            'Try
            'Chart1.ImageStorageMode = ImageStorageMode.UseImageLocation
            Using RRHHConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("M3FConnectionString").ConnectionString.ToString)

                RRHHConn.Open()


                sSql = " exec  SP_SYS_CAMBIAR_CLAVE  @Usuario,@NuevaClave ,@ClaveAnterior   ,@ClaveConfirmar "

                cmd.CommandText = sSql
                cmd.CommandType = CommandType.Text
                cmd.Connection = RRHHConn


                cmd.Parameters.AddWithValue("@Usuario", SqlDbType.VarChar)
                cmd.Parameters("@Usuario").Value = Session("stUSer")

                cmd.Parameters.AddWithValue("@NuevaClave", SqlDbType.VarChar)
                cmd.Parameters("@NuevaClave").Value = txtClaveNueva.Text

                cmd.Parameters.AddWithValue("@ClaveAnterior", SqlDbType.VarChar)
                cmd.Parameters("@ClaveAnterior").Value = txtClaveAnterior.Text

                cmd.Parameters.AddWithValue("@ClaveConfirmar", SqlDbType.VarChar)
                cmd.Parameters("@ClaveConfirmar").Value = txtClaveConfirmar.Text



                da.SelectCommand = cmd


                'cmd.ExecuteNonQuery()
                da.Fill(dsResult)

                If dsResult.Tables(0).Rows(0).Item(0) = "OK" Then
                    ClambiarClave = True


                Else
                    ClambiarClave = False
                    stError = dsResult.Tables(0).Rows(0).Item(0)

                End If
                RRHHConn.Close()
            End Using
        Catch ex As Exception
            ClambiarClave = False
            stError = Err.Description
        End Try
    End Function
End Class